/*

  A solution to the 3-3 missionary-cannibal problem

  1 for left, 0 for right is used for convenience. Original attempt was
  the opposite. Using this way you can use pure vector addition and subtraction
  to do it.

  Author: Taylor Bockman

*/
import scala.collection.immutable.{Queue, Stack}

//State-space representation (a,b,c)
//  a = # of missionaries on left side
//  b = # of cannibals on left side
//  c = boat position (1 for left, 0 for right)

case class Choice(numM: Int, numC: Int, boatP: Int, var children: Seq[Choice]){

	//Each of these returns a new state based on the current state.
	def add(m: Int, c: Int, bp: Int): Choice = Choice(numM + m, numC + c, boatP + bp, List[Choice]())
	def sub(m: Int, c: Int, bp: Int): Choice = Choice(numM - m, numC - c, boatP - bp, List[Choice]())

	//override def toString = "<" + numM + ", " + numC + ", " + boatP + ", "">"
}



object Solution {

	val cannibals = 3
	val missionaries = 3

	//Return true if the configuration is valid
	def isValid(c: Choice): Boolean = {
		if(c.numC > cannibals || c.numM > missionaries || c.numC < 0 || c.numM < 0){
			//Weird invalid state
			false
		} else if(c.numM > 0 && c.numC > c.numM){
			//println("2nd case turning down " + c)
			//More cannibals than missionaries left. Bad.
			false
		} else if(missionaries - c.numM > 0 && cannibals - c.numC > missionaries - c.numM){
			//More cannibals than missionaires on the right side. Bad.
			//println("Third case turning down " + c)
			false
		} else {
			true
		}
	}

	//Builds the children for the tree creation
	def buildChildren(n: Choice): List[Choice] = {
		//First generate the full list of choices
		if(n.boatP == 1){
			//Subtract
			//<1,0,1>, <0,1,1>, <2,0,1>, <0,2,1>, <1,1,1>
			val l = List[Choice](
				  			Choice(n.numM - 1, n.numC, n.boatP - 1, List[Choice]()),
				  			Choice(n.numM, n.numC - 1, n.boatP - 1, List[Choice]()),
				  			Choice(n.numM - 2, n.numC, n.boatP - 1, List[Choice]()),
				  			Choice(n.numM, n.numC - 2, n.boatP - 1, List[Choice]()),
				  			Choice(n.numM - 1, n.numC - 1, n.boatP - 1, List[Choice]())
				       )

			l.filter(isValid(_))
		} else {
			//Add
			//<1,0,1>, <0,1,1>, <2,0,1>, <0,2,1>, <1,1,1>
			val l = List[Choice](
	  			Choice(n.numM + 1, n.numC, n.boatP + 1, List[Choice]()),
	  			Choice(n.numM, n.numC + 1, n.boatP + 1, List[Choice]()),
	  			Choice(n.numM + 2, n.numC, n.boatP + 1, List[Choice]()),
	  			Choice(n.numM, n.numC + 2, n.boatP + 1, List[Choice]()),
	  			Choice(n.numM + 1, n.numC + 1, n.boatP + 1, List[Choice]())
	       )

			l.filter(isValid(_))
		}
	} 

	//Use BFS to build tree
	//Depth restriction used because of memory constraints
	//Is there a way to do this so it terminates properly without nuking memory?
	def buildTree(maxDepth: Int): Choice = {

		val root = Choice(missionaries,cannibals,1,List[Choice]())

		@annotation.tailrec
		def innerBuild(q: Queue[Choice], curDepth: Int, maxDepth: Int): Unit = 

			if(curDepth <= maxDepth){
					q.dequeueOption match {
						case Some((c: Choice, qt: Queue[Choice])) => {
							//Visit node
							//Build all of its children
							//Put all children onto the queue
							c.children = buildChildren(c)
							val newQ = c.children.foldLeft(qt)((b,a) => b :+ a)
							innerBuild(newQ, curDepth + 1, maxDepth)
						}

						case _ => ()
					}
			}


		innerBuild(Queue[Choice](root), 0, maxDepth)
		root
	}

	//Use DFS to solve.
	def solveTree(r: Choice): List[Choice] = {

		def innerSolve(s: Stack[Choice], backtrack: Queue[Choice]): List[Choice] = {
			if(s.size > 0){
				val top = s.pop2
				val n = top._1
				val rs = top._2

				if(backtrack.exists(a => a.numM == n.numM && a.numC == n.numC && a.boatP == n.boatP)){
					//We have seen this node before, skip it entirely.
					innerSolve(rs, backtrack)
				} else {
					//Since remaining stack still has items we have work to do
					if(n.numM == 0 && n.numC == 0 && n.boatP == 0) {
						(backtrack :+ n).toList
					} else {
						val ns = n.children.foldLeft(rs)((b,a) => b.push(a))
					  innerSolve(ns, backtrack :+ n)
					}

				}

			} else {
				//Entire tree traversed, no solution found
				List[Choice]()
			}
		}

		innerSolve(Stack[Choice](r), Queue[Choice]())
	}
	
}

